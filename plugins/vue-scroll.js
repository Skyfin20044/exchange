import Vue from 'vue'
import vuescroll from 'vuescroll'

Vue.use(vuescroll, {
  ops: {
    vuescroll: {
      mode: 'native',
    },
    scrollPanel: {
      scrollingX: false,
      scrollingY: true,
    },
    rail: {
      size: '6px',
      gutterOfEnds: '10px',
      gutterOfSide: '8px',
    },
    bar: {
      background: '#c1c1c1',
      size: '8px',
    },
  },
  name: 'CustomScroll',
})
