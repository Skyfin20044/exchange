function modalWindow() {
  const close: () => false = () => {
    return false
  }

  const show: () => true = () => {
    return true
  }

  const toggle: (a: boolean) => boolean = (state) => {
    if (state) {
      return false
    }
    return true
  }

  return {
    close,
    show,
    toggle,
  }
}

export const { close, show, toggle } = modalWindow()
